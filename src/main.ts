import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  const config = new DocumentBuilder()
    .setTitle('Trello')
    .setDescription('Task trello board')
    .setVersion('1.0')
    .addTag('trello')
    .addBasicAuth({
      in: 'header',
      type: 'http',
      bearerFormat: 'JWT'
    }, 'JWT')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);  
  
  await app.listen(4000);
}
bootstrap();
