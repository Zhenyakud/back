import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ColumnsModule } from './columns/columns.module';
import { CardsModule } from './cards/cards.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CardEntity } from './cards/entities/card.entity';
import { ColumnEntity } from './columns/entities/column.entity';
import { UserEntity } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    ColumnsModule,
    CardsModule,
    AuthModule,
    UsersModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      url: 'postgres://czobcdzc:abvacMyPRgUPZUjUjlPwc5eUky4Pfkrv@fanny.db.elephantsql.com/czobcdzc',
      entities: [CardEntity, ColumnEntity, UserEntity],
      synchronize: true,
      logging: true,
      autoLoadEntities: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
