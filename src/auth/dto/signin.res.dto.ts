import { IsJWT, IsString } from "class-validator";
import { UserEntity } from "src/users/entities/user.entity";

export class SingInResponseDto {
  @IsString()
  userId: UserEntity['id'];
  
  @IsString()
  @IsJWT()
  accessToken: string;
  
  @IsString()
  @IsJWT()
  refreshToken: string;
}
