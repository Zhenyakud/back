import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MinLength } from 'class-validator';

export class SignUpDto {
  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @MinLength(10, {
    message: 'Пароль не может быть меньше 10 символов',
  })
  @IsString()
  password: string;
}
