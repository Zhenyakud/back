import { Injectable } from '@nestjs/common';
import { CardRepository } from './cards.repository';
import { CreateCardDto } from './dto/create-card.dto';
import { UpdateCardDto } from './dto/update-card.dto';
import { CardEntity } from './entities/card.entity';
import { ColumnEntity } from 'src/columns/entities/column.entity';

@Injectable()
export class CardService {
  constructor(private cardRepository: CardRepository) {}

  async findAll(): Promise<CardEntity[]> {
    return this.cardRepository.find();
  }

  async oneCard(id: CardEntity['id']): Promise<CardEntity> {
    return this.cardRepository.findOne(id);
  }

  async updateCard(
    id: CardEntity['id'],
    dto: UpdateCardDto,
  ): Promise<CardEntity> {
    return this.cardRepository.save({ id: id, ...dto });
  }

  async createCard(dto: CreateCardDto): Promise<CardEntity> {
    return this.cardRepository.save(dto);
  }

  async removeCard(id: CardEntity['id']): Promise<boolean> {
    this.cardRepository.delete(id);
    return true;
  }
  async getColumnCards(columnId: ColumnEntity['id']): Promise<CardEntity[]> {
    return this.cardRepository.find({ where: { columnId: columnId } });
  }
}
