import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsUUID } from 'class-validator';
import { ColumnEntity } from 'src/columns/entities/column.entity';

export class CreateCardDto {
  @IsString()
  @ApiProperty()
  text: string;

  @IsUUID()
  @ApiProperty()
  columnId: string;
}
