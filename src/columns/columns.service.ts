import { Injectable } from '@nestjs/common';
import { UserEntity } from 'src/users/entities/user.entity';
import { ColumnRepository } from './columns.repository';
import { CreateColumnDto } from './dto/create-column.dto';
import { UpdateColumnDto } from './dto/update-column.dto';
import { ColumnEntity } from './entities/column.entity';

@Injectable()
export class ColumnService {
  constructor(private columnRepository: ColumnRepository) { }

  async getAllColumn(userId: UserEntity['id']): Promise<ColumnEntity[]> {
    return this.columnRepository.find({
      where: {
        userId: userId,
      },
      relations: ['cards'],
    });
  }

  async getOneColumn(id: ColumnEntity['id']): Promise<ColumnEntity> {
    return this.columnRepository.findOne(id);
  }

  async updateColumn(
    id: ColumnEntity['id'],
    dto: UpdateColumnDto,
  ): Promise<ColumnEntity> {
    return this.columnRepository.save({ id: id, ...dto });
  }

  async createColumn(
    dto: CreateColumnDto,
    userId: UserEntity['id']
  ): Promise<ColumnEntity> {
    return this.columnRepository.save({ userId, ...dto });
  }

  async removeColumn(id: ColumnEntity['id']): Promise<boolean> {
    this.columnRepository.delete(id);
    return true;
  }
}
