import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ColumnService } from './columns.service';
import { CreateColumnDto } from './dto/create-column.dto';
import { UpdateColumnDto } from './dto/update-column.dto';
import { ColumnEntity } from './entities/column.entity';
import { CardEntity } from 'src/cards/entities/card.entity';
import { CardService } from 'src/cards/cards.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { User } from 'src/users/decorators/user.decorator';
import { UserEntity } from 'src/users/entities/user.entity';
import { ApiBearerAuth, ApiBody, ApiParam, ApiTags } from '@nestjs/swagger';


@ApiTags('column')
@ApiBearerAuth('JWT')
@UseGuards(JwtAuthGuard)
@Controller('columns')
export class ColumnController {
  constructor(
    private columnService: ColumnService,
    private cardService: CardService,
  ) { }

  @Get()
  async findAllColumn(
    @User('id') userId: UserEntity['id']
    ): Promise<ColumnEntity[]> {
    return this.columnService.getAllColumn(userId);
  }

  @ApiParam({
    name: 'id',
    type: String,
  })
  @Get(':id/cards')
  async getColumnCard(
    @Param('id') id: ColumnEntity['id'],
  ): Promise<CardEntity[]> {
    return this.cardService.getColumnCards(id);
  }
  @ApiParam({
    name: 'id',
    type: String,
  })
  @Get(':id')
  async findOneColumn(
    @Param('id') id: ColumnEntity['id'],
  ): Promise<ColumnEntity> {
    return this.columnService.getOneColumn(id);
  }
  @ApiParam({
    name: 'id',
    type: String,
  })
  @ApiBody({
    type: UpdateColumnDto,
  })
  @Patch(':id')
  async updateColumn(
    @Param('id') id: ColumnEntity['id'],
    @Body() dto: UpdateColumnDto,
  ): Promise<ColumnEntity> {
    return this.columnService.updateColumn(id, dto);
  }

  @ApiBody({
    type: CreateColumnDto,
  })
  @Post()
  async createColumn(
    @User('id') userId: UserEntity['id'],
    @Body() dto: CreateColumnDto,
  ): Promise<ColumnEntity> {
    return this.columnService.createColumn(dto, userId);
  }

  @ApiParam({
    name: 'id',
    type: String,
  })
  @Delete(':id')
  async removeColumn(@Param('id') id: ColumnEntity['id']): Promise<boolean> {
    return this.columnService.removeColumn(id);
  }
}
